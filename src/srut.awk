#!/usr/bin/awk -f
# 
#  Copyright (C) 2011-2020   Michele Martone   <michelemartone _AT_ users.sourceforge.net>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.

# sparsersb update tests
# awk -f srut.awk   < sparsersb.cc  > sparsersb.cc.new
BEGIN { wp=1; of=""; }
/GENERATED TEST LINES BEGIN/ { wp=0; print; system("octave --eval 'printf(\"%s\",sparsersbtg ())'") ; }
/GENERATED TEST LINES END/ { wp=1; }
/.*/ { if(wp==1) print; }
