# 
#  Copyright (C) 2011-2020   Michele Martone   <michelemartone _AT_ users.sourceforge.net>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, see <http://www.gnu.org/licenses/>.
# 

## Original version was from the NIST "Matrix Market" service
##  https://math.nist.gov/MatrixMarket/mmio/matlab/mmiomatlab.html
## and distributed as free software in the public domain.

function [word, remainder] = gettok(string)
%
%  function [word, remainder] = gettok(string)
%
%  Retrieves the first blank separated token from the string.
%
si = findstr(string,' ');
lstring=length(string);
lsi=length(si);
if ( lsi == 0 )
   word=string;
   remainder='';
   return
end
firstb=si(1);
if ( firstb > 1 )
  word=string(1:firstb-1);
  remainder=string(firstb+1:lstring);
  return;
end
tmp=1;
while ( tmp < lsi )
  if ( si(tmp+1) == si(tmp)+1 )
     tmp=tmp+1;
  else
     break;
  end
end
if ( tmp == lstring )
  word=-1;
  remainder=string;
  return;
end
word=string(si(tmp)+1:si(tmp+1)-1);
remainder=string(si(tmp+1)+1:lstring);

    
   
